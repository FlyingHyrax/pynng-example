import cmd
import pynng

addr = "tcp://127.0.0.1:8765"

class Client(cmd.Cmd):
    connection: pynng.Pair0
    intro = "Send commands to the server - use '?' to list valid commands"
    prompt = "(cmd)> "

    def preloop(self):
        self.connection = pynng.Pair0(dial=addr, send_timeout=3000, recv_timeout=10_000)

    def postloop(self) -> None:
        self.connection.close()

    def do_open(self, _arg):
        self.connection.send(b"open")
        print(self.connection.recv())

    def do_close(self, _arg):
        self.connection.send(b"close")
        print(self.connection.recv())

    def do_server_exit(self, _arg):
        self.connection.send(b"exit")
        print(self.connection.recv())
        return True

def main():
    # nice place for argparse
    Client().cmdloop()

main()
