# Example client/server

Create a virtual environment, activate it, and install [pynng](https://pynng.readthedocs.io/en/latest/index.html) and its dependencies. On Windows (PowerShell + Windows Terminal) I did this:

```
PS> py -m venv .venv
PS> .\.venv\Scripts\Activate.ps1
(.venv) PS> python -m pip install -r requirements.txt
```

Start the server:

```
(.venv) PS> python server.py
```

_In another terminal_ - open a new tab or split the current tab so there is a second pane - activate the virtual environment, then run the client:

```
PS> .\.venv\Scripts\Activate.ps1
(.venv) PS> python client.py
```

This starts a little command loop where you can send 'open', 'close', or 'server_exit' commands to the server, and see how the server reacts by looking at its logs in the other terminal.
