import logging
import threading
import time

from typing import Callable, Optional

import pynng


log = logging.getLogger("server")

addr = "tcp://127.0.0.1:8765"


def pretend_get_prompt_audio(prompt: str):
    log.info(f"getting audio for {prompt}")
    time.sleep(0.4)
    log.info(f"playing audio for prompt {prompt}")


def pretend_get_user_speech(timeout: int):
    log.info("listening for user speech...")
    time.sleep(timeout)
    log.info("finished listening for user speech")


def start_background_chat_loop(listen_duration: int) -> Callable[[], None]:
    cancel_event = threading.Event()

    def pretend_work():
        start_time = time.time()
        while not cancel_event.is_set():
            duration = time.time() - start_time
            pretend_get_prompt_audio("example prompt")
            # could re-check cancel_event here
            pretend_get_user_speech(listen_duration)
            log.info(f"I've been doing work for {duration:0.2d} seconds!")

    background_thread = threading.Thread(target=pretend_work, name="worker")

    def do_cancel():
        cancel_event.set()
        background_thread.join()

    background_thread.start()

    return do_cancel


def command_loop():
    stop_callback: Optional[Callable[[], None]] = None
    log.info("server entered command loop")

    with pynng.Pair0(listen=addr, recv_timeout=3000, send_timeout=3000) as connection:
        log.info(f"server started listening ({addr})")

        while True:
            try:
                message = connection.recv(block=True)
            except pynng.exceptions.Timeout:
                continue

            match message:
                case b"open":
                    if stop_callback is None:
                        stop_callback = start_background_chat_loop(3)
                        log.info("started chat loop")
                    else:
                        log.warning(
                            "received 'open' command but chat loop already running"
                        )
                    connection.send(b"OK: open")

                case b"close":
                    if stop_callback is not None:
                        stop_callback()
                        stop_callback = None
                        log.info("stopped chat loop")
                    else:
                        log.warning(
                            "received 'close' command but chat loop not running"
                        )
                    connection.send(b"OK: close")

                case b"exit":
                    log.info("received 'exit' command")
                    if stop_callback is not None:
                        stop_callback()
                    connection.send(b"OK: exit")
                    return

                case unrecognized:
                    log.warning(f"received unrecognized command '{unrecognized!s}'")
                    connection.send(b"ERR: unrecognized command")
                    continue


def main():
    # can have argument parsing or other setup
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s | %(threadName)s | %(message)s",
        datefmt="%H:%M:%S",
        level=logging.DEBUG,
    )
    command_loop()

main()
